
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {

  let ime, priimek, datumRojstva;

  switch (stPacienta) {
    case 1:
      ime = "Jaka";
      priimek = "Racman";
      datumRojstva = "1934-6-9T14:30";
    case 2:
      ime = "Medved";
      priimek = "Pu";
      datumRojstva = "1921-8-21T11:26";
    default:
      ime = "Luis";
      priimek = "Pena";
      datumRojstva = "1976-1-13T03:00";
  }

  ehrId = "";

  // TODO: Potrebno implementirati

  if (ime && priimek && datumRojstva) {
    $.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              console.log(ehrId);
            }
          },
          error: function(err) {
          	console.log(err);
          }
        });
      }
		});
  }

  return ehrId;

}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
